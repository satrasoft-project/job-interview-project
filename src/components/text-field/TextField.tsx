import React, { FC, ReactNode } from 'react';

import MuiTextField, { TextFieldProps as MuiTextFieldProps } from '@mui/material/TextField';
import Box from '@mui/material/Box';

import { mergeSx } from '../../utils/styles';
import { ErrorMessage } from '../error-message/ErrorMessage';

const styles = {
  fullWidth: {
    width: '100%',
  },
};

export interface TextFieldProps {
  showError?: boolean;
  readOnly?: boolean;
  errorMessage?: string;
  label?: ReactNode;
  variant?: MuiTextFieldProps['variant'];
}

export const TextField: FC<TextFieldProps & MuiTextFieldProps> = React.forwardRef(
  ({ variant, errorMessage, showError, readOnly, margin, autoComplete, type, name, ...props }, ref) => {
    return (
      <Box sx={mergeSx(props.fullWidth ? styles.fullWidth : null)}>
        <MuiTextField
          inputRef={ref}
          {...props}
          type={autoComplete === 'off' ? 'search' : type}
          margin={margin}
          variant={variant}
          error={Boolean(errorMessage?.length)}
          autoComplete={autoComplete}
          InputProps={{
            ...(props.InputProps || {}),
            readOnly: (props.InputProps || {}).readOnly || readOnly || false,
          }}
        />
        <ErrorMessage variant={variant} show={showError} message={errorMessage} />
      </Box>
    );
  }
);

TextField.defaultProps = {
  showError: true,
  fullWidth: true,
};
