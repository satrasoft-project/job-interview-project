import React, { FC } from 'react';

import { FormHelperText, Typography } from '@mui/material';

interface ErrorMessageProps {
  show?: boolean;
  variant?: 'standard' | 'outlined' | 'filled';
  className?: string;
  message?: string | null;
}

const styles = {
  root: {
    marginTop: -0.5,
  },
};

export const ErrorMessage: FC<ErrorMessageProps> = ({ variant, className, message, show = false }) => {
  const empty = <Typography data-testid="error-message-empty-state">&nbsp;</Typography>;
  return show ? (
    <FormHelperText variant={variant} error className={className} sx={styles.root} role="error">
      {message ?? empty}
    </FormHelperText>
  ) : null;
};
