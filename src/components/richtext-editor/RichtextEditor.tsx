import React from 'react';
import { FieldValues, useController } from 'react-hook-form';

import { Box, SxProps, Theme } from '@mui/material';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import { EventInfo } from '@ckeditor/ckeditor5-utils';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import { FieldProps } from '../../models/react-hook-form.model';

interface RichtextEditorProps<T extends FieldValues> extends FieldProps<T> {
  sx?: SxProps<Theme>;
}

export const RichtextEditor = <T extends FieldValues = FieldValues>({ name, control, sx }: RichtextEditorProps<T>) => {
  const {
    field: { value, onChange },
  } = useController({
    name,
    control,
  });

  const handleEditorChange = (event: EventInfo<string, unknown>, editor: ClassicEditor) => {
    const data = editor.getData();
    onChange(data);
  };

  return (
    <Box sx={sx}>
      <CKEditor editor={ClassicEditor} data={value} onChange={handleEditorChange} />
    </Box>
  );
};
