import React from 'react';

import { Box, Typography } from '@mui/material';

interface PageProps {
  title?: string;
  children: React.ReactNode;
}

const styles = {
  container: {
    paddingTop: 3,
    boxShadow: 3,
    width: '85%',
    margin: 'auto',
    marginTop: 5,
  },
  box: {
    padding: 3,
    textAlign: 'right',
  },
  title: {
    marginLeft: 3,
  },
};

const Page = ({ children, title }: PageProps) => {
  return (
    <Box sx={styles.container}>
      {title ? (
        <Typography variant="h5" sx={styles.title}>
          {title}
        </Typography>
      ) : null}
      <Box sx={styles.box}>{children}</Box>
    </Box>
  );
};

export default Page;
