import React from 'react';

import { TableBody } from '@mui/material';

import { CompanyModel } from '../../models/company.model';
import Row from './components/Row';

interface TableBodyProps {
  tableData: CompanyModel[];
}

const TableBodyComponent = ({ tableData }: TableBodyProps) => {
  return (
    <TableBody>
      {tableData.map((data) => (
        <Row key={data.id} data={data} />
      ))}
    </TableBody>
  );
};

export default TableBodyComponent;
