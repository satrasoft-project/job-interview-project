import * as React from 'react';
import { useTranslation } from 'react-i18next';

import { TableCell, TableHead, TableRow } from '@mui/material';

interface TableHeaderProps {
  header: string[];
  show: boolean;
}

const styles = {
  headerCell: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
};

const TableHeadComponent = ({ header, show }: TableHeaderProps) => {
  const { t } = useTranslation();

  return (
    <TableHead>
      <TableRow>
        {show && <TableCell />}
        {header.map((item) => (
          <TableCell key={item} sx={styles.headerCell}>
            {t(item)}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default TableHeadComponent;
