import * as React from 'react';

import { Table, TableContainer, Paper } from '@mui/material';

import { TableDataModel } from '../../models/tabelData.model';
import TableBodyComponent from './TableBody';
import TableHeadComponent from './TableHead';

interface MuiTableProps {
  data: TableDataModel;
}

const styles = {
  table: {
    minWidth: 600,
  },
};

const MuiTable = ({ data }: MuiTableProps) => {
  return (
    <TableContainer component={Paper}>
      <Table sx={styles.table} aria-label="collapsible table">
        <TableHeadComponent header={data.header} show />
        <TableBodyComponent tableData={data.body} />
      </Table>
    </TableContainer>
  );
};

export default MuiTable;
