import { Fragment, useState } from 'react';

import { Box, IconButton, TableCell, TableRow } from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

import { CompanyModel } from '../../../models/company.model';
import ExpandedRow from './ExpandedRow';

interface RowProps {
  data: CompanyModel;
}

const styles = {
  bodyCell: {
    textAlign: 'center',
  },
  row: {
    '&:last-child td, &:last-child th': { border: 0 },
    '& > *': { borderBottom: 'unset' },
  },
};

const Row = ({ data }: RowProps) => {
  const [open, setOpen] = useState(false);

  return (
    <Fragment>
      <TableRow sx={styles.row}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell sx={styles.bodyCell}>{data.name}</TableCell>
        <TableCell sx={styles.bodyCell}>{data.email}</TableCell>
        <TableCell sx={styles.bodyCell}>{data.employeeNumber}</TableCell>
        <TableCell sx={styles.bodyCell}>
          {data.description && <Box dangerouslySetInnerHTML={{ __html: data.description }} />}
        </TableCell>
      </TableRow>

      <ExpandedRow isOpen={open} employeeData={data.employees} companyName={data.name} />
    </Fragment>
  );
};

export default Row;
