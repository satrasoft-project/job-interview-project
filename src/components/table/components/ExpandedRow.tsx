import * as React from 'react';
import { useTranslation } from 'react-i18next';

import { Box, Collapse, IconButton, Table, TableBody, TableCell, TableRow, Typography } from '@mui/material';
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';

import { EmployeeModel } from '../../../models/employee.model';
import TableHeadComponent from '../TableHead';

interface ExpandedBodyContentProps {
  companyName: string;
  employeeData: EmployeeModel[];
  isOpen: boolean;
}

const TableHeaders = ['TITLES.NAME', 'TITLES.AGE', 'TITLES.EMAIL', 'TITLES.JOB_TITLE', 'TITLES.CV'];

const styles = {
  mainCell: {
    paddingBottom: 0,
    paddingTop: 0,
  },
  bodyCell: {
    textAlign: 'center',
  },
  boxMargin: {
    margin: 2,
  },
  table: {
    boxShadow: 2,
  },
};

const ExpandedRow = ({ employeeData, companyName, isOpen }: ExpandedBodyContentProps) => {
  const { t } = useTranslation();

  const downloadCv = (cv: File) => {
    const url = URL.createObjectURL(cv);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', cv.name);

    document.body.appendChild(link);
    link.click();

    document.body.removeChild(link);
    URL.revokeObjectURL(url);
  };

  return (
    <TableRow>
      <TableCell style={styles.mainCell} colSpan={6}>
        <Collapse in={isOpen} timeout="auto" unmountOnExit>
          <Box sx={styles.boxMargin}>
            <Typography variant="h6" gutterBottom component="div">
              {`${companyName} ${t('COMMON.EMPLOYEES')}`}
            </Typography>

            <Table size="small" sx={styles.table}>
              <TableHeadComponent header={TableHeaders} show={false} />
              <TableBody>
                {employeeData.map((employee, index) => (
                  <TableRow key={index}>
                    <TableCell sx={styles.bodyCell}>{employee.employeeName}</TableCell>
                    <TableCell sx={styles.bodyCell}>{employee.age}</TableCell>
                    <TableCell sx={styles.bodyCell}>{employee.employeeEmail}</TableCell>
                    <TableCell sx={styles.bodyCell}>{t(employee.jobTitle)}</TableCell>
                    <TableCell sx={styles.bodyCell}>
                      <IconButton onClick={() => downloadCv(employee.cv as File)}>
                        <PictureAsPdfIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </Collapse>
      </TableCell>
    </TableRow>
  );
};

export default ExpandedRow;
