import React from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import i18n from 'i18next';

import { Button } from '@mui/material';
import { yupResolver } from '@hookform/resolvers/yup';

import { CompanyModel } from '../../models/company.model';
import { EmployeeModel } from '../../models/employee.model';
import { companyService } from '../../services/company.service';
import Page from '../../components/page/Page';
import CompaniesForm from './components/CompaniesForm';

const styles = {
  container: {
    textAlign: 'center',
  },
  button: {
    marginTop: 3,
    minWidth: { xs: 50, sm: 50, md: 150 },
    height: { xs: 35, sm: 35, md: 40 },
  },
};

const companyValidationSchema = Yup.object().shape({
  name: Yup.string().required(() => i18n.t('ERRORS.FIELD_REQUIRED')),
  email: Yup.string()
    .email(() => i18n.t('ERRORS.EMAIL_INVALID'))
    .required(() => i18n.t('ERRORS.FIELD_REQUIRED')),
  employeeNumber: Yup.number()
    .positive(() => i18n.t('ERRORS.NUMBER_POSITIVE'))
    .integer(() => i18n.t('ERRORS.TYPE_NUMBER'))
    .required(() => i18n.t('ERRORS.FIELD_REQUIRED'))
    .min(1, ({ min }: any) => i18n.t('ERRORS.MIN_VALUE', { min }))
    .max(100, ({ max }: any) => i18n.t('ERRORS.MAX_VALUE', { max })),
  description: Yup.string().optional(),
  employees: Yup.array()
    .required(() => i18n.t('ERRORS.FIELD_REQUIRED'))
    .of(
      Yup.object().shape({
        employeeName: Yup.string().required(() => i18n.t('ERRORS.FIELD_REQUIRED')),
        employeeEmail: Yup.string()
          .email(() => i18n.t('ERRORS.EMAIL_INVALID'))
          .required(() => i18n.t('ERRORS.FIELD_REQUIRED')),
        age: Yup.number()
          .positive(() => i18n.t('ERRORS.NUMBER_POSITIVE'))
          .integer(() => i18n.t('ERRORS.TYPE_NUMBER'))
          .required(() => i18n.t('ERRORS.FIELD_REQUIRED'))
          .min(18, ({ min }: any) => i18n.t('ERRORS.MIN_AGE', { min })),
        jobTitle: Yup.string().required(() => i18n.t('ERRORS.FIELD_REQUIRED')),
        cv: Yup.mixed<File>().required(() => i18n.t('ERRORS.FIELD_REQUIRED')),
      })
    ),
});

const CreatePage = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const defaultValues: CompanyModel = {
    name: '',
    email: '',
    employeeNumber: 1,
    description: '',
    employees: [
      { employeeName: '', employeeEmail: '', age: 18, jobTitle: '', cv: new File([], '') },
    ] as EmployeeModel[],
  };

  const { control, handleSubmit, watch } = useForm<CompanyModel>({
    values: defaultValues,
    resetOptions: { keepTouched: true },
    resolver: yupResolver(companyValidationSchema),
    mode: 'onTouched',
  });

  const employeeNo = watch('employeeNumber');

  const onSubmit = async (values: CompanyModel) => {
    await companyService.createCompany(values);
    navigate(`/home`, { replace: true });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Page title={t('TITLES.COMPANY_TITLE')}>
        <CompaniesForm control={control} employeeNo={employeeNo} />
        <Button type="submit" sx={styles.button} variant="contained">
          {t('COMMON.ADD')}
        </Button>
      </Page>
    </form>
  );
};

export default CreatePage;
