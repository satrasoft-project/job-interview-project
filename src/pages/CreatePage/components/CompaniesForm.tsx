import React, { useEffect } from 'react';
import { Control, useController } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { Grid } from '@mui/material';

import { EmployeeModel } from '../../../models/employee.model';
import { CompanyModel } from '../../../models/company.model';
import { ControlledTextField } from '../../../components/text-field/ControlledTextField';
import { RichtextEditor } from '../../../components/richtext-editor/RichtextEditor';
import EmployeesForm from './EmployeesForm';

export interface CompaniesFormProps {
  control: Control<CompanyModel, any>;
  employeeNo: number;
}

const styles = {
  mainGrid: {
    justifyContent: 'center',
  },
  textField: {
    textAlign: 'center',
    margin: 'auto',
    marginTop: 3,
    marginBottom: 1,
  },
  textArea: {
    textAlign: 'center',
    margin: 'auto',
    marginTop: 3,
    marginBottom: 3,
    '.ck-content': { height: 223 },
  },
  cardGrid: {
    '.MuiGrid-root': {
      marginX: 'auto',
      justifyContent: 'center',
      display: 'flex',
    },
  },
};

const CompaniesForm = ({ control, employeeNo }: CompaniesFormProps) => {
  const { t } = useTranslation();

  const {
    field: { value: employees, onChange },
  } = useController({
    name: `employees`,
    control,
  });

  useEffect(() => {
    const addEmployees = () => {
      const diff = employeeNo - employees.length;
      if (diff > 0) {
        const newEmployees = Array.from({ length: diff }, () => ({
          employeeName: '',
          employeeEmail: '',
          age: 18,
          jobTitle: '',
          cv: new File([], ''),
        }));
        onChange([...employees, ...newEmployees]);
      } else if (diff < 0) {
        onChange(employees.slice(0, employeeNo));
      }
    };

    addEmployees();
  }, [employeeNo]);

  const handleChange = (index: number, fieldName: keyof EmployeeModel, value: any) => {
    const updatedEmployees = [...employees];
    (updatedEmployees[index] as any)[fieldName] = value;
    onChange(updatedEmployees);
  };

  return (
    <>
      <Grid container columnSpacing={4} sx={styles.mainGrid}>
        <Grid item xs={12} sm={12} md={6}>
          <ControlledTextField
            type="text"
            name="name"
            control={control}
            label={t('TITLES.NAME')}
            sx={styles.textField}
          />
          <ControlledTextField
            type="text"
            name="email"
            control={control}
            label={t('TITLES.EMAIL')}
            sx={styles.textField}
          />
          <ControlledTextField
            type="number"
            name="employeeNumber"
            control={control}
            label={t('TITLES.EMPLOYEE_NUMBER')}
            sx={styles.textField}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <RichtextEditor name="description" control={control} sx={styles.textArea} />
        </Grid>
      </Grid>
      <Grid container columnSpacing={4} sx={styles.cardGrid}>
        {employees.map((employee, index) => (
          <Grid item xs={12} sm={12} md={6} lg={4} xl={3} key={index}>
            <EmployeesForm employeeData={employee} index={index} handleChange={handleChange} control={control} />
          </Grid>
        ))}
      </Grid>
    </>
  );
};

export default CompaniesForm;
