import React, { useState } from 'react';
import { Control } from 'react-hook-form';
import { Trans, useTranslation } from 'react-i18next';

import { Card, CardContent, CardMedia, MenuItem, Typography } from '@mui/material';

import { EmployeeModel } from '../../../models/employee.model';
import { CompanyModel } from '../../../models/company.model';
import { ControlledTextField } from '../../../components/text-field/ControlledTextField';
import { FileUpload } from '../../../components/file-upload/FileUpload';
import { arrayify } from '../../../utils/arrays';

import EmployeeImg from '../../../../public/images/user.jpg';

const styles = {
  card: {
    marginTop: 3,
    width: 350,
  },
  img: {
    maxWidth: 150,
    margin: 'auto',
  },
  textField: {
    textAlign: 'center',
    margin: 'auto',
    marginTop: 1.5,
    marginBottom: 1,
  },
  fileName: {
    textAlign: 'left',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
};

interface EmployeesFormProps {
  employeeData: EmployeeModel;
  index: number;
  control: Control<CompanyModel, any>;
  handleChange: (index: number, fieldName: keyof EmployeeModel, value: any) => void;
}

const EmployeesForm = ({ employeeData, index, handleChange, control }: EmployeesFormProps) => {
  const { t } = useTranslation();
  const [fileValue, setFileValue] = useState<File>(new File([], ''));

  const handleFileUpload = (file: File[]) => {
    handleChange(index, 'cv', file[0]);
    setFileValue(file[0]);
  };

  const acceptedFiles = {
    'application/pdf': [],
  };

  return (
    <Card sx={styles.card} key={index}>
      <CardMedia component="img" image={EmployeeImg} alt="..." sx={styles.img} />
      <CardContent>
        <ControlledTextField
          type="text"
          name={`employees.${index}.employeeName`}
          control={control}
          value={employeeData.employeeName}
          onChange={(e) => handleChange(index, `employeeName`, e.target.value)}
          label={t('TITLES.NAME')}
          sx={styles.textField}
        />
        <ControlledTextField
          type="text"
          name={`employees.${index}.employeeEmail`}
          control={control}
          value={employeeData.employeeEmail}
          onChange={(e) => handleChange(index, 'employeeEmail', e.target.value)}
          label={t('TITLES.EMAIL')}
          sx={styles.textField}
        />
        <ControlledTextField
          type="number"
          name={`employees.${index}.age`}
          control={control}
          value={employeeData.age}
          onChange={(e) => handleChange(index, 'age', e.target.value)}
          label={t('TITLES.AGE')}
          sx={styles.textField}
        />
        <ControlledTextField
          select
          name={`employees.${index}.jobTitle`}
          control={control}
          value={employeeData.jobTitle}
          onChange={(e) => handleChange(index, 'jobTitle', e.target.value)}
          label={t('TITLES.JOB_TITLE')}
          sx={styles.textField}
        >
          <MenuItem value={'OPTIONS.ACCOUNTANT'}>
            <Trans i18nKey="OPTIONS.ACCOUNTANT" />
          </MenuItem>
          <MenuItem value={'OPTIONS.SOFTWARE_DEVELOPER'}>
            <Trans i18nKey="OPTIONS.SOFTWARE_DEVELOPER" />
          </MenuItem>
          <MenuItem value={'OPTIONS.SOFTWARE_TESTER'}>
            <Trans i18nKey="OPTIONS.SOFTWARE_TESTER" />
          </MenuItem>
          <MenuItem value={'OPTIONS.MANAGER'}>
            <Trans i18nKey="OPTIONS.MANAGER" />
          </MenuItem>
        </ControlledTextField>
        <FileUpload
          onDropAccepted={handleFileUpload}
          accept={acceptedFiles}
          valueDisplayNode={arrayify(fileValue).map((file: File) => (
            <Typography key={file.name} sx={styles.fileName}>
              {file.name}
            </Typography>
          ))}
        />
      </CardContent>
    </Card>
  );
};

export default EmployeesForm;
