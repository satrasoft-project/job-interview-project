import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { Box, Button, Typography } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

import { TableDataModel } from '../../models/tabelData.model';
import { companyService } from '../../services/company.service';
import MuiTable from '../../components/table/Table';
import Page from '../../components/page/Page';

const TableHeaders = ['TITLES.NAME', 'TITLES.EMAIL', 'TITLES.EMPLOYEE_NUMBER', 'TITLES.DESCRIPTION'];

const styles = {
  button: {
    backgroundColor: 'grey',
    '&: hover': { backgroundColor: 'grey' },
    float: 'right',
    marginBottom: 2,
  },
  root: {
    width: '80%',
    height: '60vh',
    marginLeft: 'auto',
    marginRight: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'left',
  },
};

const HomePage = () => {
  const [tableData, setTableData] = useState<TableDataModel>({
    header: TableHeaders,
    body: [],
  });

  const { t } = useTranslation();
  const navigate = useNavigate();

  const emptyTable = tableData.body.length === 0;

  useEffect(() => {
    const fetchCompanies = async () => {
      const companies = await companyService.getCompanyList();
      setTableData({ header: TableHeaders, body: companies.data });
    };

    fetchCompanies();
  }, []);

  const goToCreatePage = () => {
    navigate(`/create_new_company`, { replace: true });
  };

  return (
    <Box>
      <Page title={t('TITLES.HOMEPAGE')}>
        <Button variant="contained" startIcon={<AddIcon />} sx={styles.button} onClick={goToCreatePage}>
          {t('COMMON.ADD')}
        </Button>
        {!emptyTable ? (
          <MuiTable data={tableData} />
        ) : (
          <Box sx={styles.root}>
            <Typography variant="h4">{t('NOT_FOUND.TABLE_DATA')}</Typography>
            <Typography>{t('NOT_FOUND.TABLE_DATA_DESCRIPTION')}</Typography>
          </Box>
        )}
      </Page>
    </Box>
  );
};

export default HomePage;
