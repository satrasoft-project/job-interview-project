import { FC } from 'react';
import { useTranslation } from 'react-i18next';

import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

const styles = {
  root: {
    width: '80%',
    height: '100vh',
    marginLeft: 'auto',
    marginRight: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
};

export const NotFound: FC = () => {
  const { t } = useTranslation();
  return (
    <Box data-testid="not-found" sx={styles.root}>
      <Typography variant="h4">{t('NOT_FOUND.TITLE')}</Typography>
      <Typography>{t('NOT_FOUND.DESCRIPTION')}</Typography>
    </Box>
  );
};
