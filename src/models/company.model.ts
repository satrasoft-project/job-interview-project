import { EmployeeModel } from './employee.model';

export interface CompanyModel {
  id?: number;
  name: string;
  email: string;
  employeeNumber: number;
  description?: string;
  employees: EmployeeModel[];
}
