import { FieldPath, Control, FieldValues, RegisterOptions } from 'react-hook-form';

export interface FieldProps<T extends FieldValues = FieldValues> {
  name: FieldPath<T>;
  control: Control<T>;
  rules?: RegisterOptions<T>;
}
