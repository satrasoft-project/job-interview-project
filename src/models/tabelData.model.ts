import { CompanyModel } from './company.model';

export interface TableDataModel {
  header: string[];
  body: CompanyModel[];
}
