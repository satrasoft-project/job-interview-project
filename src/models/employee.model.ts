export interface EmployeeModel {
  id?: number;
  employeeName: string;
  employeeEmail: string;
  age: number;
  jobTitle: string;
  cv: File;
}
