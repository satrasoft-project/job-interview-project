export interface ListResponseModel<D> {
  data: D[];
  count: number;
}

export interface SuccessResponseModel {
  success: 'ok';
}
