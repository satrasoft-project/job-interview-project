import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

import HomePage from './pages/HomePage/HomePage';
import { NotFound } from './pages/NotFoundPage/NotFoundPage';
import { LanguageSelect } from './components/language-select/LanguageSelect';
import CreatePage from './pages/CreatePage/CreatePage';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <LanguageSelect />
      </header>
      <Routes>
        <Route path="/" element={<Navigate to="/home" />} />
        <Route path="/home" element={<HomePage />} />
        <Route path="/create_new_company" element={<CreatePage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
