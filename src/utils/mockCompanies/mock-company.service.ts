import { CompanyModel } from '../../models/company.model';
import { ListResponseModel } from '../../models/respone.model';

export class MockCompanyService {
  companies: CompanyModel[];

  constructor() {
    this.companies = [];
  }

  async createCompany(data: Partial<CompanyModel>) {
    const newCompany = {
      ...data,
      id: data.id || 0,
      name: data.name || '',
      email: data.email || '',
      employeeNumber: data.employeeNumber || 0,
      description: data.description || '',
      employees: data.employees || [],
    };

    this.companies.push(newCompany);

    return newCompany;
  }

  async getCompanyList() {
    return {
      data: this.companies,
    } as ListResponseModel<CompanyModel>;
  }
}

export const mockCompanyService = new MockCompanyService();
