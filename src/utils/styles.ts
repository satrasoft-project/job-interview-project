import type { SxProps, Theme } from '@mui/material/styles';

import { arrayify } from './arrays';

export function mergeSx(...sx: (SxProps<Theme> | null | undefined)[]) {
  return sx.map((style) => [...arrayify<SxProps<Theme>>(style)]).flat() as SxProps<Theme>;
}
