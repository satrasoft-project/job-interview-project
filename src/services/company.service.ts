import { CompanyModel } from '../models/company.model';

import { EntityService } from '../utils/entityService';
import { mockCompanyService } from '../utils/mockCompanies/mock-company.service';

class CompanyService extends EntityService {
  constructor() {
    super('companies');
  }

  async createCompany(data: Partial<CompanyModel>) {
    return mockCompanyService.createCompany(data);
  }

  getCompanyList = async () => {
    return mockCompanyService.getCompanyList();
  };
}

export const companyService = new CompanyService();
