SatraSoft test Project dokumentáció

Rövid leírás:
A projektben egy webalkalmazást valósítottam meg, amiben cégek és az alkalmazottak adatait lehet rögzíteni.

A Főoldalon üres állapotban egy felirat utasítja fel a felhasználót, hogy rögzítsen egy új cég adatot. A "Felvétel" gombra kattintva a react-router-dom segítségével átnavigálhatunk az űrlapra ahol a cég, illetve az alkalmazottak adatai megadhatók. A mezőkre validációs sémákat hoztam létre a Yup könyvtár használatával. A cég leírása egy FreeHTML editor-ban adható meg, ahol egyedi formázásokat illeszthetünk a szövegre.

Az alkalmazottak adataira szolgáló kártyák dinamikusan jönnek létre, az "Alkalmazottak száma" című mező értéke alapján. Az alkalmazottak feltölthetik az önéletrajzukat is egy fájl feltöltő mezőnek köszönhetően. Kitöltés után a "Felvitel" gombra kattintva az adatok JSON formában mentődnek el.

A Főoldalon ha már szerepel legalább egy cég adat, táblázat formájában jelenik meg. A táblázat kinyitható funkcióval is rendelkezik és a kinyitott részben szerepelnek a dolgozók adatai. A feltöltött önéletrajzok le is tölthetőek a "PDF" ikonra kattintva.

A weboldal további funkciói közé sorolható, hogy kétnyelvű (magyar, angol), amit az i18next segítségével oldottam meg. Jobb felső sarokban szerepel egy legördülő lista, amiben van lehetőség a nyelvet kiválasztani és az oldal tartalma automatikusan lefordításra kerül.

A jobb felhasználói élmény elérése érdekében, ügyeltem a reszponzív megvalósításra is.

Program elindítása:

1. lépés: npm install
2. lépés: npm run dev

Továbbfejlesztési ötletek:
A cég és alkalmazottak adatait lehessen módosítani illetve törölni is.

Készítette:
Szakonyi Ádám
